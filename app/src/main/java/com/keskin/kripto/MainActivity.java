package com.keskin.kripto;

import android.app.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import android.content.*;
import org.apache.commons.codec.binary.*;
import org.apache.commons.codec.*;
public class MainActivity extends Activity 
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
		
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
		SharedPreferences sp = getSharedPreferences("settings",MODE_PRIVATE);
		EditText otele =(EditText) findViewById(R.id.otele);
		EditText kat =(EditText) findViewById(R.id.kat);
		otele.setText(sp.getString("otele","0"));
	    kat.setText(sp.getString("kat","1"));
		
    }
	public void cript(View view){
		EditText kat = (EditText) findViewById(R.id.kat);
		int katx =Integer.parseInt((kat.getText().toString()));
		EditText inedit =(EditText) findViewById(R.id.inedit);
		EditText outedit =(EditText) findViewById(R.id.outedit);
		EditText otele = (EditText) findViewById(R.id.otele);
		EditText hexedit =(EditText) findViewById(R.id.hexedit);
		EditText binedit = (EditText) findViewById(R.id.binedit);
		String code ="";
		String codeh="";
		String codeb ="";
		String listh=inedit.getText().toString();
		String list =inedit.getText().toString();
		String listb =inedit.getText().toString();
		int i=0;
		int num =0;
		int s=0;
		while (s < katx){
			while ( i < list.length()){
			    num = list.toCharArray()[i]+Integer.parseInt(otele.getText().toString());
			    code=code+num+" ";
			    i++;
		    }
		    i=0;
		    while ( i < listh.length()){
	    	    num = listh.toCharArray()[i]+Integer.parseInt(otele.getText().toString());
			    codeh=codeh+ Integer.toHexString(num) +" ";
			    i++;
		}
		i=0;
			while ( i < listb.length()){
	    	    num = listb.toCharArray()[i]+Integer.parseInt(otele.getText().toString());
			    codeb=codeb+ Integer.toBinaryString(num) +" ";
			    i++;
			}
		list = code;
		listh=codeh;
		listb=codeb;
		codeh="";
		code="";
		codeb="";
		i=0;
		s++;
		}
		binedit.setText(listb);
		hexedit.setText(listh);
		outedit.setText(list);
	}
	public void decript(View view){
		EditText inedit =(EditText) findViewById(R.id.inedit);
		EditText otele = (EditText) findViewById(R.id.otele);
		EditText binedit =(EditText) findViewById(R.id.binedit);
		EditText outedit =(EditText) findViewById(R.id.outedit);
		EditText hexedit =(EditText) findViewById(R.id.hexedit);
		String code ="";
		String tmp = outedit.getText().toString();
		
		int i=0;
		String num = "" ;
		EditText kat = (EditText) findViewById(R.id.kat);
		int katx =Integer.parseInt((kat.getText().toString()));
		int s=0;
		while (s < katx){
		tmp=tmp.trim();
		tmp.replace("\n","");
		tmp.replace("  "," ");
		String list[] = tmp.split(" ");
	    while ( i < list.length){
			try{
			    num= Character.toString((char)(Integer.parseInt(list[i])-Integer.parseInt(otele.getText().toString())));
				code=code+num;
				i++;
		    }
		    catch(NumberFormatException nfe)
		    {
		     	return  ;
		    }
		  
		}
		tmp=code;
		i=0;
		code="";
		s++;
	}
		inedit.setText(tmp.trim());
		hexedit.setText("");
		binedit.setText("");
		
	}
		
	public void clctxt(View view){
		EditText inedit = (EditText) findViewById(R.id.inedit);
		inedit.setText("");
	}
	public void clcpass(View view){
		EditText outedit = (EditText) findViewById(R.id.outedit);
		outedit.setText("");
	}
	public void clchex(View view){
		EditText hexedit = (EditText) findViewById(R.id.hexedit);
		hexedit.setText("");
	}
	public void clcbin(View view){
		EditText binedit = (EditText) findViewById(R.id.binedit);
		binedit.setText("");
	}
	

	@Override
	protected void onStop()
	{
		// TODO: Implement this method
		
		EditText otele =(EditText) findViewById(R.id.otele);
		EditText kat =(EditText) findViewById(R.id.kat);
		SharedPreferences.Editor sp = getSharedPreferences("settings",MODE_PRIVATE).edit();
		sp.putString("kat",kat.getText().toString().trim());
		sp.putString("otele",otele.getText().toString().trim());
		sp.commit();
		super.onDestroy();
	}
	public void hexadec(View view){
		EditText inedit =(EditText) findViewById(R.id.inedit);
		EditText otele = (EditText) findViewById(R.id.otele);
		EditText hexedit =(EditText) findViewById(R.id.hexedit);
		EditText binedit =(EditText) findViewById(R.id.binedit);
		EditText outedit = (EditText) findViewById(R.id.outedit);
		String code ="";
		String tmp = hexedit.getText().toString();

		int i=0;
		String num = "" ;
		EditText kat = (EditText) findViewById(R.id.kat);
		int katx =Integer.parseInt((kat.getText().toString()));
		int s=0;
		while (s < katx){
			tmp=tmp.trim();
			tmp.replace("\n","");
			tmp.replace("  "," ");
			String list[] = tmp.split(" ");
			while ( i < list.length){
				try{
					num= Character.toString((char)Integer.parseInt(Integer.toString(Integer.parseInt(list[i],16)-Integer.parseInt(otele.getText().toString()))));
					code=code+num;
					i++;
				}
				catch(NumberFormatException nfe)
				{
					return  ;
				}

			}
			tmp=code;
			i=0;
			code="";
			s++;
		}
		inedit.setText(tmp.trim());
		outedit.setText("");
		binedit.setText("");
		
	}
	public void binar(View view){
		EditText inedit =(EditText) findViewById(R.id.inedit);
		EditText otele = (EditText) findViewById(R.id.otele);
		EditText hexedit =(EditText) findViewById(R.id.hexedit);
		EditText binedit =(EditText) findViewById(R.id.binedit);
		EditText outedit = (EditText) findViewById(R.id.outedit);
		String code ="";
		String tmp = binedit.getText().toString();

		int i=0;
		String num = "" ;
		EditText kat = (EditText) findViewById(R.id.kat);
		int katx =Integer.parseInt((kat.getText().toString()));
		int s=0;
		while (s < katx){
			tmp=tmp.trim();
			tmp.replace("\n","");
			tmp.replace("  "," ");
			String list[] = tmp.split(" ");
			while ( i < list.length){
				try{
					num= Character.toString((char)Integer.parseInt(Integer.toString(Integer.parseInt(list[i],2)-Integer.parseInt(otele.getText().toString()))));
					code=code+num;
					i++;
				}
				catch(NumberFormatException nfe)
				{
					return  ;
				}

			}
			tmp=code;
			i=0;
			code="";
			s++;
		}
		inedit.setText(tmp.trim());
		outedit.setText("");
		hexedit.setText("");
	}
	
}

